<?php
defined('_JEXEC') or die();

?>
<h2 class="iit-title"><?php echo $lng == 'it' ? 'Programmi di ricerca' : 'Programs'?>
    <a	href="<?php echo modIitPrograms::buildProgramUlrFromAlias('programs')?>"
		title="<?php echo $lng == 'it' ? 'Tutti i programmi': 'All programs'?>">
		<span class="fa fa-angle-right pull-right"></span>
	</a>
</h2>
<section id="programs" class="programs">
    <div class="row">
        <?php foreach ($programs as $index => $program):?>
        <?php $link = modIitPrograms::buildProgramUlrFromAlias($program->{$alias})?>
            <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-0">
    			<div class="iit_prg_media_type">
    			 <a href="<?php echo $link?>">
    				<img class="mod-prg-img" alt=""
    					src="<?php echo $program->img_source?>" />
    			 </a>
    			</div>
    			<a	href="<?php echo $link?>">
    			   <h4	class="" style="font-weight: bold;"><?php echo $program->{$name} ;?></h4>
    			</a>
    			<a href="<?php echo $link?>"><p class="prg-description"><?php echo $program->{$description}?></p></a>
    		</div>
        <?php endforeach;?>
        <div style="clear: both;"></div>
    </div>
</section>
<div class="row">
	<h5 id="programs-label"
		class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 all-programs-label">
		<span class="str-to-upper"><?php echo $lng == 'it' ? 'vedi tutti i programmi' : 'see all programs' ?></span>
		<span class="fa fa-angle-down pull-right"></span>
	</h5>
</div>