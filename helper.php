<?php
defined('_JEXEC') or die();

abstract class modIitPrograms
{
    public static function  getPrograms()
    {
        $config = JFactory::getConfig();
        $apiBaseUrl = $config->get('api_host');
        $http = new JHttp();
        $apiResponse = $http->get($apiBaseUrl.'/relation/programs');
        if ($apiResponse->code == 200){
            $rawData = json_decode($apiResponse->body);
            $programs = $rawData->_embedded->program;
            $selected = array();
            $pCount = count($programs);
            
            while (count($selected) < 3)
            {
                $rand = rand(0, $pCount-1);
                if (!in_array($rand , $selected)) {                                     
                    $tmp = $programs[count($selected)];
                    $programs[count($selected)] = $programs[$rand];
                    $programs[$rand] = $tmp;
                    
                    $selected[] = $rand;
                }
            }
            return $programs;
        }
        
        return false;
        
    }
    
    public static function buildProgramUlrFromAlias($alias) 
    {
        $artId = 0;
        $catId = 0;
        $itemId = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id,catid')
              ->from($db->quoteName('#__content'))
              ->where($db->quoteName('alias') . ' = '. $db->quote($alias));
        $db->setQuery($query);

        $results = $db->loadObject();
        if ($results){
            $artId = $results->id;
            $catId = $results->catid;
        }
        else {
            return '#';
        }
        $query = $db->getQuery(true);
        $query->select('id')
              ->from($db->quoteName('#__menu'))
              ->where($db->quoteName('alias').' = '.$db->quote($alias))
              ;
        $db->setQuery($query);
        $results = $db->loadObject();
        if ($results){
            $itemId = $results->id;
        }
        else {
            return '#';
        }
        
        return JRoute::_("index.php?option=com_content&view=article&id=$artId&catid=$catId&Itemid=$itemId");
            
    }
}