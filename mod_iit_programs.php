<?php
defined('_JEXEC') or die();
require __DIR__.'/helper.php';

$lng = (JFactory::getLanguage()->getTag() == 'it-IT') ? 'it' : 'en';

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base()."modules/mod_iit_programs/css/iitprograms.css");
#$doc->addStyleSheet("mod_iit_programs/css/iitprograms.css",true);

$label = ($lng == 'it') ? "comprimi" :"collapse programs";
$label_expand = $lng == "it" ? "vedi tutti i programmi" :"see all programs";

$doc->addScriptDeclaration('
    $(document).ready(function() {
    $("#programs-label").click(function(e) {
    if ( $("#programs").is(".programs")) {
        $("#programs").removeClass("programs");
        $("#programs-label .fa").removeClass("fa-angle-down");
        $("#programs-label .fa").addClass("fa-angle-up");
         $(".str-to-upper").html("'.$label.'");
    }
    else {
        $("#programs").addClass("programs");
        $("#programs-label .fa").removeClass("fa-angle-up");
        $("#programs-label .fa").addClass("fa-angle-down");
        $(".str-to-upper").html("'.$label_expand .'");
    }
    e.preventDefault();
  });
});
');

$programs = modIitPrograms::getPrograms();


$name = 'name_'.$lng;
$alias = 'alias_'.$lng;
$description = 'description_'.$lng;


require JModuleHelper::getLayoutPath('mod_iit_programs','default'); 